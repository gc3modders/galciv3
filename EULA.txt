Stardock Entertainment End User License Agreement

Important: Read this before using your copy of Stardock Entertainment's Galactic Civilizations III.

NOTICE: This Game utilizes StardockGames.com Services.  By installing this Game, you also agree and are bound to the Terms of the StardockGames.com Service.  The most current version of the StardockGames.com Terms are located at: www.stardockgames.com/terms.

This user license agreement (the AGREEMENT) is an agreement between you (individual or single entity) and Stardock Entertainment (Stardock) for Galactic Civilizations III (the SOFTWARE) that is accompanying this AGREEMENT.

The SOFTWARE is the property of Stardock and is protected by copyright laws and international copyright treaties. The SOFTWARE is not sold, it is licensed.

LICENSED VERSION: The LICENSED VERSION means a Registered Version (using your personal serial/registration number) or an original fully working version of the SOFTWARE. If you accept the terms and conditions of this AGREEMENT, you have certain rights and obligations as follow:

YOU MAY:
1. Install and use copies of the SOFTWARE on any computer you own.
2. Create "mods" based on the documented functions of the SOFTWARE to customize it. Distribution of your Mod in any form constitutes a grant by you to Stardock of an irrevocable, perpetual, royalty-free, sub-licensable right to use, copy, modify and distribute that Mod (and derivatives of that Mod), and use your name if we choose to, for any purpose and through any means, and without obligation to pay you anything, obtain your approval, or give you credit. You also agree to promptly execute assignments confirming this license upon request from Stardock.

YOU MAY NOT:
1. Copy and distribute the SOFTWARE or any portion of it except as expressly provided in this Agreement.
2. Sublicense, rent, lease or transfer your personal serial number without express written consent from Stardock.
3. Sublicense, rent or lease the SOFTWARE or any portion of it.
4. Decompile, disassemble, reverse engineer or modify the SOFTWARE or any portion of it, or make any attempt to bypass, unlock, or disable any protective or initialization system on the SOFTWARE.
5. Copy the documentation accompanying the SOFTWARE.
6. Upload or transmit the SOFTWARE, or any portion thereof, to any electronic bulletin board, network, or other type of multi-use computer system regardless of purpose (except as provided for above for "mods").
7. Include the SOFTWARE in any commercial products intended for manufacture, distribution, or sale.
8. Include "mods" in any commercial products intended for manufacture, distribution, or sale.

WARRANTY DISCLAIMER
The SOFTWARE is supplied "AS IS". Stardock disclaims all warranties, expressed or implied, including, without limitation, the warranties of merchantability and of fitness for any purpose. The user must assume the entire risk of using this SOFTWARE.

DISCLAIMER OF DAMAGES
Stardock Entertainment assumes no liability for damages, direct or consequential, which may result from the use of this SOFTWARE, even if Stardock Entertainment may have been advised of the possibility of such damages. Any liability will be limited to refund of the purchase price.

TERM
This license is effective from your date of purchase and shall remain in force until terminated. You may terminate the license and this agreement at any time by destroying the SOFTWARE and its documentation, together with all copies in any form.

COPYRIGHT NOTICE
The Company and/or our Licensors hold valid copyright in the Software. Nothing in this Agreement constitutes a waiver of any rights under U.S. Copyright law or any other federal, state or international law.

ACKNOWLEDGMENT: YOU ACKNOWLEDGE THAT YOU HAVE READ THIS AGREEMENT, UNDERSTAND IT AND AGREE TO BE BOUND BY ITS TERMS AND CONDITIONS. YOU ALSO AGREE THAT THIS AGREEMENT IS THE COMPLETE AND EXCLUSIVE STATEMENT OF THE AGREEMENT BETWEEN YOU AND THE COMPANY AND SUPERCEDES ALL PROPOSALS OR PRIOR ENDORSEMENTS, ORAL OR WRITTEN, AND ANY OTHER COMMUNICATIONS BETWEEN YOU AND THE COMPANY OR ANY REPRESENTATIVE OF THE COMPANY RELATING TO THE SUBJECT MATTER OF THIS AGREEMENT.

Notice: Product offered subject to your acceptance of the Steam Subscriber Agreement ("SSA"). You must activate this product via the Internet by registering for a Steam account and accepting the SSA. Please see http://www.steampowered.com/agreement to view the SSA prior to purchase. If you do not agree with the provisions of the SSA, you should return this game unopened to your retailer in accordance with their return policy.
 
Published by:
Stardock Entertainment
15090 N Beck Road
Plymouth, MI 48170 USA
http://www.stardock.com

Stardock Entertainment is a registered trademark of Stardock Systems, Inc. Galactic Civilizations is a registered trademark of Stardock Entertainment. All rights reserved.

©2015 Stardock Entertainment.

All trademarked names mentioned in this document and SOFTWARE are used for editorial purposes only, with no intention of infringing upon the trademarks.

No part of this publication may be reproduced without written permission from Stardock Entertainment.

All rights reserved.

©2015 Valve Corporation. Steamworks and the Steamworks logo are trademarks and/or registered trademarks of Valve Corporation in the U.S. and/or other countries.

Monotype is a trademark of Monotype Imaging Inc. registered in the U.S. Patent & Trademark Office and may be registered in certain jurisdictions.
Bank Gothic is a trademark of MyFonts and may be registered in certain jurisdictions.
 
---
MONOTYPE® EULA for Bank Gothic™ Font Use

By installing and using the Product, you agree to the following terms and conditions.

l.	The Product contains font software programs which generate human readable typeface designs ("Font Software"). You may not install or use the Font Software on any device except one on which you have installed a properly licenced copy of the Product.

2.	The Font Software is supplied to you for Internal Use only. "Internal Use," as used herein, means use (i) in the course of your customary and ordinary internal business, or (ii) for your personal use. If used in the course of your customary and ordinary internal business, Internal Use shall mean use solely by your authorized agents and employees. If used for personal use, Internal Use shall mean use solely by individuals who reside with you in your household. All such agents, employees and household residents must agree to the terms and conditions of this EULA as a condition of using the Font Software. Internal Use shall occur when an individual is able to give commands (whether by keyboard or otherwise) that are followed by the Font Software, regardless of the location in which the Font Software resides.

3.	You may not convert the Font Software into a different format. You may not alter or modify the Font Software in any manner which results in the Font Software having different or enhanced functionality then when it was delivered to you as part of the Product.

4.	You may use an application program such as Adobe Acrobat to embed the Font Software into an electronic document. You may send such an electronic document to a third party only for the purpose of permitting the third party to view and print the electronic document. Font Software may not be embedded in any format which permits the recipient of an electronic document to install the Font Software or to use the Font Software for any purpose beyond merely viewing and printing the document. You may not embed Font Software into a Commercial Product. A "Commercial Product" is an electronic document which is distributed in exchange for a fee or other consideration. For example, you cannot embed Font Software into an electronic book or magazine which is offered to the public for a fee.

5.	Except for the print and view embedding permission granted in paragraph 4 above, you may not copy the Font Software, provided, however, you may make one copy of the Font Software for archival purposes only. The archival copy cannot be distributed and can be used only when you have permanently deleted the original or any copy of the Font Software on your device. You may not reverse engineer, decompile, or take any action which results in or designed to result in gaining access to the source code of the Font Software, except as permitted by law and then only for the purpose of achieving an interoperable program.

6.	The Font Software supplied with the Product is proprietary and is protected by U.S. and international copyright and trademark law. All rights not expressly set forth herein are reserved. A breach of this End User Licence Agreement may subject you to damages and injunctive relief under this Agreement as well as under applicable copyright and trademark law.

7.	YOU AGREE THAT THE FONT SOFTWARE IS SUPPLIED TO YOU WITHOUT ANY WARRANTIES, EXPRESSED OR IMPLIED, INCLUDING WITHOUT LIMlTATION ANY WARRANTIES OF FITNESS FOR A PARTICULAR PURPOSE OR MERCHANT ABILITY. You agree that the Font Software is supplied without any warranty that the Font Software is free of all bugs, errors, and omissions. YOU AGREE THAT IN NO EVENT WlLL THE PROVIDER OF THE PRODUCT OR ITS SUPPLIERS, INCLUDING  THE SUPPLIER(S) OF THE FONT SOFTWARE, BE LIABLE TO YOU OR ANY OTHER PARTY FOR LOST PROFITS, LOST DATA, OR ANY OTHER  INDIRECT, INCIDENTAL OR CONSEQUENTIAL DAMAGES, OR FOR ANY PUNITIVE OR SPECIAL DAMAGES, EVEN IF YOU NOTIFIED THE PROVIDER OF THE PRODUCT AND THE  SUPPLIERS OF THE FONT SOFTWARE OF THE POSSIBILITY OF SUCH DAMAGES. You agree that your sole and exclusive remedy and the sole liability of the provider of the Product and the supplier(s) of the Font Software for defective Font Software is, upon return of the defective Font Software to the provider of the Product, either (and at the sole option of the provider of the Product (i) the replacement of defective Font Software or (ii) the refund of your Licensee fee paid for such Font Software. Some jurisdictions do not allow the exclusion or Limitations  of incidental, consequential or special damages, so the above exclusion may not apply to you. Also, some jurisdictions  do not allow the  exclusion of implied warranties or limitations on how long an implied warranty may last, so the above limitations may not apply to you. To  the greatest extent permitted by law, any implied warranties not effectively excluded by this Agreement are limited to ninety (90) days.  Some jurisdictions do not permit a limitation or exclusion of implied warranties where the product results in physical injury or death so that such limjtations or exclusions may not apply to you. In those jurisdictions, you agree that the liability of the supplier of the Font  Software for such physical injury shall not exceed one hundred thousand dollars (U.S. $100,000), provided that such jurisdictions permit a limitation of such liability. This warranty gives you specific legal rights. You may have other rights that vary from jurisdiction to  jurisdiction. Other than as expressly set forth herein, the Font Software is non-returnable and nonrefundable.

8.	This licence shall remain in effect so long as you are in material compliance with all of its terms and conditions. If you breach any of the terms and conditions, this licence is automatically terminated and you are obligated to destroy the original and all copies of the Font Software. In such event, upon the request of the provider of the Product or the suppliers of the Font Software, you shall provide written certification of such destruction.

9.	If you are acquiring Font Software on behalf of any unit or agency of the United States Government, the following provisions shall apply. Use, duplication or disclosure by the United States Government is subject to restrictions as set forth in the Rights in Technical Data and computer Software clause at FAR 252.227-7013, subdivision (b)(3)(ii) or subparagraph (c)(l)(ii), as appropriate. Further use, duplication or disclosure is subject to restrictions to restricted rights software as set forth in FAR 52.227-19(c)(2).
