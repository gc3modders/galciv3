INSTALLING MODS:

Install mods by copying the mod directory into this location.
"My Documents Location"\My Games\GalCiv3\Mods. It should be
the location where you are reading this file.


DEVELOPING MODS:

When creating new mods follow the file structure demonstrated
in the ExampleMod directory. The game will read the directories here
in the same way it reads them in the install directory. Each 
mod must have at least:

ParticleScriptDefs
Text         
Core         
Game

These directories correspond with the directories found in the install path
GalCiv3\Game\Data\. Each type of xml file must appear in its corresponding
mod path in order to function correctly.


XML TIPS:

You will be mod xml in two different ways.

Total Conversion Method:
The first way is to copy an xml file from the corresponding install
directory and paste it into your mod directory. The game will match
the file name with the file from the install directory, and use your
modded version in it's place. This will let you modify, add, or remove 
existing xml. If multiple mods are installed that attempt to mod the same 
xml file, only the file that is loaded last is used.


Append Method:
However, if you only want to add new xml you should use the second method.
This is done by creating new xml files that are uniquely named that match the 
formatting of the xml you intend to append to. The advantage to this style
is that you can create mods that can be combined with other mods that
simply append new xml in this style.
