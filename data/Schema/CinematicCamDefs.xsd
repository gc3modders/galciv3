﻿<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema">
  <xs:include schemaLocation="Lib/CustomTypes.xsd"/>

  <xs:complexType name="CinematicCameraRange">
    <xs:sequence>
      <xs:element name="Min" type="xs:float" minOccurs="1" default="0"/>
      <xs:element name="Max" type="xs:float" minOccurs="1" default="0"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="CinematicCameraRadiusZoom">
    <xs:sequence>
      <xs:element name="ZoomDelay" type="xs:float" minOccurs="1" maxOccurs="1" default="100"/>
      <xs:element name="ZoomStart" type="xs:float" minOccurs="1" maxOccurs="1" default="100"/>
      <xs:element name="ZoomEnd" type="xs:float" minOccurs="1" maxOccurs="1" default="100"/>
      <xs:element name="PostZoomDelay" type="xs:float" minOccurs="1" maxOccurs="1" default="0"/>
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="CinematicCameraFocus">
    <xs:sequence>
      <xs:element name="FocusDelay" type="xs:float" minOccurs="1" maxOccurs="1" default="0"/>
      <xs:element name="FocusStart" type="xs:float" minOccurs="1" maxOccurs="1" default="0"/>
      <xs:element name="FocusEnd" type="xs:float" minOccurs="1" maxOccurs="1" default="0"/>
      <xs:element name="PostFocusDelay" type="xs:float" minOccurs="1" maxOccurs="1" default="0"/>
    </xs:sequence>
  </xs:complexType>
  
  <xs:simpleType name="CinematicKeyValues">
    <xs:restriction base="xs:string">
      
      <!-- Corners of Bound Box -->
      <xs:enumeration value="RandomCorner1"/>
      <xs:enumeration value="RandomCorner2"/>
      <xs:enumeration value="RandomFrontCorner1"/>
      <xs:enumeration value="RandomFrontCorner2"/>
      <xs:enumeration value="RandomBackCorner1"/>
      <xs:enumeration value="RandomBackCorner2"/>
      <xs:enumeration value="RandomTopCorner1"/>
      <xs:enumeration value="RandomTopCorner2"/>
      <xs:enumeration value="RandomBottomCorner1"/>
      <xs:enumeration value="RandomBottomCorner2"/>
      <xs:enumeration value="Corner1"/>
      <xs:enumeration value="Corner2"/>
      <xs:enumeration value="Corner3"/>
      <xs:enumeration value="Corner4"/>
      <xs:enumeration value="Corner5"/>
      <xs:enumeration value="Corner6"/>
      <xs:enumeration value="Corner7"/>
      <xs:enumeration value="Corner8"/>
      
      <!-- Points on Bound Box -->
      <xs:enumeration value="Center"/>
      <xs:enumeration value="TopCenter"/>
      <xs:enumeration value="BottomCenter"/>
      <xs:enumeration value="RightCenter"/>
      <xs:enumeration value="LeftCenter"/>
      <xs:enumeration value="FrontCenter"/>
      <xs:enumeration value="BackCenter"/>
            
      <!-- Ship Positions -->
      <xs:enumeration value="PrimaryShip"/>
      <xs:enumeration value="SecondaryShip"/>
      
      <!-- Ship Relative Positions -->
      <xs:enumeration value="PrimaryShipForward"/>
      <xs:enumeration value="SecondaryShipForward"/>      
      
      <!-- Light Positions -->
      <xs:enumeration value="KeyLight"/>
      
      <!-- Camera Position -->
      <xs:enumeration value="CameraPosition"/>
      
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="CinematicTags">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Generic"/>
      <xs:enumeration value="Intro"/>
    </xs:restriction>
  </xs:simpleType>

  <xs:simpleType name="CinematicMotionTypes">
    <xs:restriction base="xs:string">
      <xs:enumeration value="Stationary"/>
      <xs:enumeration value="Linear"/>
      <xs:enumeration value="FollowPrimaryTarget"/>
      <xs:enumeration value="FollowSecondaryTarget"/>
    </xs:restriction>
  </xs:simpleType>
    
  <xs:complexType name="CinematicOrbitData">
    <xs:sequence>
      <xs:element name="Radius" type="CinematicCameraRange" minOccurs="1" maxOccurs="1"/>
      <xs:element name="RadiusZoom" type="CinematicCameraRadiusZoom" minOccurs="0" maxOccurs="1"/>
      <xs:element name="StartRotation" type="CinematicCameraRange" minOccurs="1" maxOccurs="1"/>
      <xs:element name="RotationSpeed" type="CinematicCameraRange" minOccurs="1" maxOccurs="1"/>
    </xs:sequence>
  </xs:complexType>
    
  <xs:complexType name="CinematicCameraDef">
    <xs:sequence>

      <xs:element name="InternalName" type="xs:string"/>
      <xs:element name="Likelihood" type="FixedDecimal" minOccurs="1" maxOccurs="1"/>
      <xs:element name="MotionType" type="CinematicMotionTypes" minOccurs="1" maxOccurs="1"/>
      <xs:element name="StartPosition" type="CinematicKeyValues" minOccurs="0" maxOccurs="1" default="Center"/>
      <xs:element name="EndPosition" type="CinematicKeyValues" minOccurs="0" maxOccurs="1" default="Center"/>
      <xs:element name="StartPositionOffset" type="Vector3" minOccurs="0" maxOccurs="1"/>
      <xs:element name="EndPositionOffset" type="Vector3" minOccurs="0" maxOccurs="1"/>
      <xs:element name="CameraPositionOffset" type="Vector3" minOccurs="0" maxOccurs="1"/>
      <xs:element name="PrimaryTarget" type="CinematicKeyValues" minOccurs="1" maxOccurs="1"/>
      <xs:element name="SecondaryTarget" type="CinematicKeyValues" minOccurs="1" maxOccurs="1"/>
      <xs:element name="PrimaryTargetOffset" type="Vector3" minOccurs="0" maxOccurs="1"/>
      <xs:element name="SecondaryTargetOffset" type="Vector3" minOccurs="0" maxOccurs="1"/>
      <xs:element name="Orbit" type="CinematicOrbitData" minOccurs="1" maxOccurs="1"/>
      <xs:element name="MinShipsRequired" type="xs:int" minOccurs="0" default="0"/>
      <xs:element name="MaxShipsRequired" type="xs:int" minOccurs="0" default="9999"/>
      <xs:element name="Duration" type="CinematicCameraRange" minOccurs="1" maxOccurs="1"/>
      <xs:element name="BoundBoxScale" type="Vector3" minOccurs="1" maxOccurs="1"/>
      <xs:element name="CameraFocusEnable" type="xs:boolean" minOccurs="1" maxOccurs="1" default="true"/>
      <xs:element name="CameraFocusNearStart" type="CinematicCameraFocus" minOccurs="1" maxOccurs="1"/>
      <xs:element name="CameraFocusNearEnd" type="CinematicCameraFocus" minOccurs="1" maxOccurs="1"/>
      <xs:element name="CameraFocusFarStart" type="CinematicCameraFocus" minOccurs="1" maxOccurs="1"/>
      <xs:element name="CameraFocusFarEnd" type="CinematicCameraFocus" minOccurs="1" maxOccurs="1"/>
      <xs:element name="DOFGatherBlurSize" type="xs:float" minOccurs="1" default="4.4"/>
      <xs:element name="BokehBrightnessThreshold" type="xs:float" minOccurs="1" default="1.0"/>
      <xs:element name="BokehBlurThreshold" type="xs:float" minOccurs="1" default="1.0"/>
      <xs:element name="BokehMaxSize" type="xs:float" minOccurs="1" default="10.0"/>
      <xs:element name="BokehFallOff" type="xs:float" minOccurs="1" default="1.0"/>
      <xs:element name="VignetteEnabled" type="xs:boolean" minOccurs="0" default="false"/>                  
      <xs:element name="Tag" type="CinematicTags" minOccurs="1" maxOccurs="unbounded"/>                  
    </xs:sequence>
  </xs:complexType>

  <xs:complexType name="CinematicCamListDef">
    <xs:sequence>
      <xs:element name="CinematicCamera" type="CinematicCameraDef" maxOccurs="unbounded"/>
    </xs:sequence>
  </xs:complexType>

  <xs:element name="CinematicCameraList" type="CinematicCamListDef"/>

</xs:schema>