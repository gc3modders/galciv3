# README #

This README covers how to download, use, and modify various 
Mods to the Galactic Civilizations III game, as published
and licensed by StarDock Eterntainment, Inc.

### Repository Location ###

This repository is available at:   https://bitbucket.org/gc3modders/galciv3


### StarDock Entertainment, Inc Website ###

http://www.stardock.com/


### Galactic Civilizations III Website ###

http://www.galciv3.com

This is the general information site for the game.

### Online Forums for GC3 ###

http://forums.galciv3.com/

These contain all discussion boards, including support, bugs,
modding hints, and various forward-looking feature discussions.

### Modding-Specific Forums ###

https://forums.galciv3.com/forum/1131

This board is specifically for people interested in producing
a Mod of their own.


### How do I get set up? ###

This repository contains *only* the various XML customization files 
for the base game, the various DLC, and Expansion Packs.

It does NOT contain the graphics, images, animations, and other files 
necessary to run the actual game or pack in question. You *must* install
the relevant game version and/or DLC or Expansion prior to downloading
any of these files.

You should look in your own home directory for the following path:

My Documents\My Games

The *GalCiv3* folder therein is where all Mods to the base game go.
This includes any DLC intended to be played with the base game, as well
as Expansion Pack 1: Mercenaries.

The *GC3Crusade* folder contains the Expansion Pack 2: Crusade data, 
as well as any modifications to DLC that are needed to be compatible 
with Crusade.

All branches EXCEPT the EXP2-Crusade branch should be copied into the 
*GalCiv3* folder. The latter should be cloned into the *GC3Crusade* folder.

It is recommended that you clone this repository somewhere else, pulling 
down the appropriate branches into a working set. You can then copy that 
working set into a custom-named directory under the relevant directory above.
E.g. "TestMod"


### Contribution guidelines ###

Please refer to the *EULA.txt* file in the base distribution for copyright,
licensing, and other legal requirements. 


### Who do I talk to? ###

Please see the Modding forum above for more discussion.

